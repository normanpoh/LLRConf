%% This main file contains a walk-through tutorial illustrating what you can do with the library.
% This is fairly rudimentary.
%
% Author: Norman Poh (normanpoh@ieee.org)
% Date  : 2017-06-05


%% First load the data
load main_llr_confidence.mat

%% Add path
addpath Lib

%% Get the configuration for the 13 experiments
b_=0;
for p=1:2
  for b=1:size(data{p}.dset{1,1},2)
    b_=b_+1;
    info(b_,:) = [p b];
  end;
end;

%% Pick one experiment
% We have 13 experiments and we choose just one for illustration here
b_=4; % experiment 4 out of 13 is chosen
p = info(b_,1);
b = info(b_,2);

fprintf(1,'Running experiment %s, based on Lausanne Protocol %d, experiment %d\n', ...
  syslabel{b_}, p, b);

%% Do the sample-level bootstrapping
clear com model;
LOGISTIC=1;
KDE=2;
SAMPLE_BS=1;
SUBJECT_BS=2;

tic;
for s = 1:100  %Perform 100 bootstraps, each time fitting using methods 1 or 2  
  nexpe = draw_samples(data{p}, b);
 
  % METHOD 1 -- logistic regression
  [~, ~,com_] = calibrate_logistic2(nexpe.dset{1,1}, nexpe.dset{1,2},1);
  model{b_}{SAMPLE_BS,LOGISTIC}.param(:,s) = com_.b;
      
  % METHOD 2 -- KDE
  [~, ~,com_] = calibrate_LLR(nexpe.dset{1,1}, nexpe.dset{1,2},1);
  model{b_}{SAMPLE_BS,KDE}{s} = com_;
  
  fprintf(1,'.');
end;
fprintf(1,'\n');toc

%% Do the subject-level bootstrapping

tic;
for s = 1:100 %Perform 100 bootstraps, each time fitting using methods 1 or 2  
  selected_user = randi(200,1,200);
  nexpe = filter_users(data{p}, b, selected_user);
  
  % METHOD 1: logistic regression
  [~, ~,com_] = calibrate_logistic2(nexpe.dset{1,1}, nexpe.dset{1,2},1);
  model{b_}{SUBJECT_BS,LOGISTIC}.param(:,s) = com_.b;
  
  % METHOD 2: KDE
  [~, ~,com_] = calibrate_LLR(nexpe.dset{1,1}, nexpe.dset{1,2},1);
  model{b_}{SUBJECT_BS,KDE}{s} = com_;
  fprintf(1,'.');
end;
fprintf(1,'\n');toc
display('Subject level bootstrapping takes longer because we need to subsample the users first.');

%% To undertand what happens, we plot the distributions of the same-origin versus different-origin pairs for KDE (using sample-level bootstraping)

figure(1); clf; hold on;
for s=1:100
  plot(model{b_}{SAMPLE_BS,KDE}{s}.x, model{b_}{SAMPLE_BS,KDE}{s}.pdf{1},'r-')
  plot(model{b_}{SAMPLE_BS,KDE}{s}.x, model{b_}{SAMPLE_BS,KDE}{s}.pdf{2},'b-')
end;
title('Sample-level bootstraping');

% Record the raw score space for subsequent use
axis_=axis;
xspace = linspace(axis_(1),axis_(2),100); 

%% In the original space using subject-level bootstraping for KDE
figure(2);clf; hold on;
for s=1:100
  plot(model{b_}{SUBJECT_BS,KDE}{s}.x, model{b_}{SAMPLE_BS,KDE}{s}.pdf{1},'r-')
  plot(model{b_}{SUBJECT_BS,KDE}{s}.x, model{b_}{SAMPLE_BS,KDE}{s}.pdf{2},'b-')
end;
title('Subject-level bootstraping');
axis(axis_);

%% In the likelihood space -- so we make inference with KDE here
clear out_sample

for bs=[SAMPLE_BS, SUBJECT_BS]
  likelihoods{b_,bs} = infer_KDE_xspace(model{b_}{bs,KDE}, xspace);
end;

%% A quick plot of KDE in the likelihood space
figure;
boxplot(likelihoods{b_,SAMPLE_BS}(1:5:end,:)','outliersize', 1);

%% A better plot of KDE in the likelihood space
figure(3); clf; 
plot_likelihood_xspace(xspace, likelihoods{b_,SUBJECT_BS},0,1);
title('Subject-level bootstrapping');

figure(4); clf; 
plot_likelihood_xspace(xspace, likelihoods{b_,SAMPLE_BS},0,1);
title('Sample-level bootstrapping');

%% Compare the two KDE
figure(5);
plot_likelihood_xspace(xspace, likelihoods{b_,SAMPLE_BS},0);
plot_likelihood_xspace(xspace, likelihoods{b_,SUBJECT_BS},1);
legend('sample','sample (median)','sample','subject','subject (median)','subject', 'location', 'southeast');
display('The subject-level bootstrapping has slightly larger confidence intervals');

%% Now, we focus on logistic regression by viewing its variability in the parameter space
close all;
clf; hold on;
signs_ = {'b+', 'ro'};
for bs=[SAMPLE_BS, SUBJECT_BS]
  param_ = model{b_}{bs,LOGISTIC}.param;
  plot(param_(1,:),param_(2,:),signs_{bs});
end;
legend('Sample', 'Subject');
xlabel('\omega_0');
ylabel('\omega_1');
display('The subject-level bootstrapping has slightly larger variability in the parametric space');

%% View the variability due to logistic regression in the raw score space
title_bs = {'Sample-level bootstrap', 'Subject-level bootstrap'};
for bs=[SAMPLE_BS, SUBJECT_BS]
  param_ = model{b_}{bs,LOGISTIC}.param;
  for s=1:size(param_,2)
    Prob(:,s) = glmval(param_(:,s), xspace, 'logit');
  end;
  figure(bs); clf; hold on;
  plot(xspace, Prob);
  title(title_bs{bs});
end;

%% View the variability due to logistic regression in the likelihood space

for bs=[SAMPLE_BS, SUBJECT_BS]
  likelihoods_LOGISTIC{b_,bs} = infer_LOGISTIC_xspace(model{b_}{bs,LOGISTIC}.param, xspace);
end;

close all;
figure(1); hold on;

for bs=[SAMPLE_BS, SUBJECT_BS]
  plot_likelihood_xspace(xspace, likelihoods_LOGISTIC{b_,bs},bs-1);
end;
legend('sample','sample (median)','sample','subject','subject (median)','subject', 'location', 'southeast');
display('The subject-level bootstrapping has slightly larger confidence intervals.');

%%
close all; figure;
b_=4; % experiment 4 out of 13 is chosen
p = info(b_,1);
b = info(b_,2);
xbound_error = plot_histogram(xspace, data{p}.dset{1,2}(:,b), data{p}.dset{1,1}(:,b));
printfig('histogram04', 'main_llr_confidence_tutorial');



%% Compare the likelihoods with two methods using subject-level bootstrapping
close all; 
subplot(4,1,[1 2 3]);
hold on;
ptile{LOGISTIC} = plot_likelihood_xspace(xspace, likelihoods_LOGISTIC{b_,SUBJECT_BS},0);
ptile{KDE} = plot_likelihood_xspace(xspace, likelihoods{b_,SUBJECT_BS},1);
display('The logistic regression is actually consistent with KDE for the most part of the intervals. The consistent region is plotted below.');


% Compute the overlap and does the plotting at the same time with an
% overalpping threshold of 0.5
axis_ = axis; xlim_ = axis_(1:2);
[overlap_,~,union_, xbound_] = calculate_intervals_overlap(ptile,axis_,xspace, 0.5);
xlabel('');
title(syslabel{b_});
ylim([-5, 5]);

%subplot(4,1,3);
%plot(xspace,overlap_);
%legend('logistic','logistic (median)','logistic','KDE','KDE (median)','KDE', 'consistent region','location', 'southeast');
%ylabel('Jaccard');

subplot(4,1,4);
plot_histogram(xspace, data{p}.dset{1,2}(:,b), data{p}.dset{1,1}(:,b), xbound_);
xlim(xlim_);
