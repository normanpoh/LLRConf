REFERENCES
----------
If you use the toolbox, please cite:
* Norman Poh, Firham M. Senan, Nazri A. Zamani, and M. Zaharudin A. Darus, The LLRConf Toolbox for Biometric Forensics: Toward an evidence-based, consistent, repeatable and replicable likelihood ratio computation framework, submitted.

To cite the data set, please cite:
* N. Poh and S. Bengio, Database, Protocol and Tools for Evaluating Score-Level Fusion Algorithms in Biometric Authentication, in Pattern Recognition Journal, Vol. 39, Issue 2, pages 223-233, Feb 2006.

For the specific protocols of using 13 experiments, they were introduced in the following paper, which you should cite if you use main_llr_confidence.mat.
* N. Poh, A. Ross, W. Li, and J. Kittler, A User-Specific and Selective Multimodal Biometric Fusion Strategy by Ranking Subjects, Pattern Recognition 46(12): 3341-57, 2013.
For more information, please visit:
https://sites.google.com/site/llrconftoolbox

LICENCE 
-------
Please review the following licence before downloading the software.
Reporting likelihood ratio reliably � The LLRConf Toolbox version 0.1
&copy; University of Surrey 2017 (the �Software�)

The Software is and remains the property of the University of Surrey (�University�). The Licensee will ensure that the Copyright Notice set out above appears prominently wherever the Software is used.
The Software is distributed under this Licence:
*	on a non-exclusive basis, 
*	solely for non-commercial use in the hope that it will be useful, 
*	�AS-IS� and in order that the University as a charitable foundation protects its assets for the benefit of its educational and research purposes, the University makes clear that no condition is made or to be implied, nor is any representation or warranty given or to be implied, as to (i) the quality, accuracy or reliability of the Software; (ii) the suitability of the Software for any particular use or for use under any specific conditions; and (iii) whether use of the Software will infringe third-party rights. 

The University disclaims:
*	all responsibility for the use which is made of the Software; and 
*	any liability for the outcomes arising from using the Software. 
The Licensee may make public, results or data obtained from, dependent on or arising out of the use of the Software provided that any such publication includes a prominent statement identifying the Software as the source of the results or the data, including the Copyright Notice and stating that the Software has been made available for use by the Licensee under licence from the University and the Licensee provides a copy of any such publication to the University.

The Licensee agrees to indemnify the University and hold them harmless from and against any and all claims, damages and liabilities asserted by third parties (including claims for negligence) which arise directly or indirectly from the use of the Software or any derivative of it or the sale of any products based on the Software. The Licensee undertakes to make no liability claim against any employee, student, agent or appointee of the University, in connection with this Licence or the Software.

No part of the Software may be reproduced, modified, transmitted or transferred in any form or by any means, electronic or mechanical, without the express permission of the University. The University�s permission is not required if the said reproduction, modification, transmission or transference is done without financial return, the conditions of this Licence are imposed upon the receiver of the product, and all original and amended source code is included in any transmitted product. You may be held legally responsible for any copyright infringement that is caused or encouraged by your failure to abide by these terms and conditions.

You are not permitted under this Licence to use this Software commercially. Use for which any financial return is received shall be defined as commercial use, and includes (1) integration of all or part of the source code or the Software into a product for sale or license by or on behalf of Licensee to third parties or (2) use of the Software or any derivative of it for research with the final aim of developing software products for sale or license to a third party or (3) use of the Software or any derivative of it for research with the final aim of developing non-software products for sale or license to a third party, or (4) use of the Software to provide any service to an external organisation for which payment is received. If you are interested in using the Software commercially, please contact the University to negotiate a licence. Contact details are: .
<a href="mailto:techtransfer@surrey.ac.uk?Subject=LLRConf%20Software" target="_top">techtransfer@surrey.ac.uk</a> or <a href="mailto:N.Poh@surrey.ac.uk?Subject=LLRConf%20Software" target="_top">N.Poh@surrey.ac.uk</a>.