%% Use the main function to generate figure 1 and many other figures appeared in the submitted journal paper.
% Author: Norman Poh (normanpoh@ieee.org)
% Date  : 2017-06-05

%% First load the data
load main_llr_confidence.mat

%% Add path
addpath Lib

%% Get the configuration for the 13 experiments
b_=0;
for p=1:2
  for b=1:size(data{p}.dset{1,1},2)
    b_=b_+1;
    info(b_,:) = [p b];
  end;
end;

%% Pick one experiment
% We have 13 experiments and we choose just one for illustration here
b_=4; % experiment 4 out of 13 is chosen
p = info(b_,1);
b = info(b_,2);

fprintf(1,'Running experiment %s, based on Lausanne Protocol %d, experiment %d\n', ...
  syslabel{b_}, p, b);

%% Do the sample-level bootstrapping
clear com model;
LOGISTIC=1;
KDE=2;
SAMPLE_BS=1;
SUBJECT_BS=2;

tic;
for s = 1:100  %Perform 100 bootstraps, each time fitting using methods 1 or 2  
  nexpe = draw_samples(data{p}, b);
 
  % METHOD 1 -- logistic regression
  [~, ~,com_] = calibrate_logistic2(nexpe.dset{1,1}, nexpe.dset{1,2},1);
  model{b_}{SAMPLE_BS,LOGISTIC}.param(:,s) = com_.b;
      
  % METHOD 2 -- KDE
  [~, ~,com_] = calibrate_LLR(nexpe.dset{1,1}, nexpe.dset{1,2},1);
  model{b_}{SAMPLE_BS,KDE}{s} = com_;
  
  fprintf(1,'.');
end;
fprintf(1,'\n');toc

%% Do the subject-level bootstrapping

tic;
for s = 1:100 %Perform 100 bootstraps, each time fitting using methods 1 or 2  
  selected_user = randi(200,1,200);
  nexpe = filter_users(data{p}, b, selected_user);
  
  % METHOD 1: logistic regression
  [~, ~,com_] = calibrate_logistic2(nexpe.dset{1,1}, nexpe.dset{1,2},1);
  model{b_}{SUBJECT_BS,LOGISTIC}.param(:,s) = com_.b;
  
  % METHOD 2: KDE
  [~, ~,com_] = calibrate_LLR(nexpe.dset{1,1}, nexpe.dset{1,2},1);
  model{b_}{SUBJECT_BS,KDE}{s} = com_;
  fprintf(1,'.');
end;
fprintf(1,'\n');toc
display('Subject level bootstrapping takes longer because we need to subsample the users first.');

%% To undertand what happens, we plot the distributions of the same-origin versus different-origin pairs for KDE (using sample-level bootstraping)

figure(1); clf; hold on;
for s=1:100
  plot(model{b_}{SAMPLE_BS,KDE}{s}.x, model{b_}{SAMPLE_BS,KDE}{s}.pdf{1},'r-')
  plot(model{b_}{SAMPLE_BS,KDE}{s}.x, model{b_}{SAMPLE_BS,KDE}{s}.pdf{2},'b-')
end;
title('Sample-level bootstraping');

% Record the raw score space for subsequent use
axis_=axis;
xspace = linspace(axis_(1),axis_(2),100); 

%% In the original space using subject-level bootstraping for KDE
figure(2);clf; hold on;
for s=1:100
  plot(model{b_}{SUBJECT_BS,KDE}{s}.x, model{b_}{SAMPLE_BS,KDE}{s}.pdf{1},'r-')
  plot(model{b_}{SUBJECT_BS,KDE}{s}.x, model{b_}{SAMPLE_BS,KDE}{s}.pdf{2},'b-')
end;
title('Subject-level bootstraping');
axis(axis_);

%% In the likelihood space -- so we make inference with KDE here
clear out_sample

for bs=[SAMPLE_BS, SUBJECT_BS]
  likelihoods{b_,bs} = infer_KDE_xspace(model{b_}{bs,KDE}, xspace);
end;

%% A quick plot of KDE in the likelihood space
figure;
boxplot(likelihoods{b_,SAMPLE_BS}(1:5:end,:)','outliersize', 1);

%% A better plot of KDE in the likelihood space
figure(3); clf; 
plot_likelihood_xspace(xspace, likelihoods{b_,SUBJECT_BS},0,1);
title('Subject-level bootstrapping');

figure(4); clf; 
plot_likelihood_xspace(xspace, likelihoods{b_,SAMPLE_BS},0,1);
title('Sample-level bootstrapping');

%% Compare the two KDE
figure(5);
plot_likelihood_xspace(xspace, likelihoods{b_,SAMPLE_BS},0);
plot_likelihood_xspace(xspace, likelihoods{b_,SUBJECT_BS},1);
legend('sample','sample (median)','sample','subject','subject (median)','subject', 'location', 'southeast');
display('The subject-level bootstrapping has slightly larger confidence intervals');

%% Now, we focus on logistic regression by viewing its variability in the parameter space
close all;
clf; hold on;
signs_ = {'b+', 'ro'};
for bs=[SAMPLE_BS, SUBJECT_BS]
  param_ = model{b_}{bs,LOGISTIC}.param;
  plot(param_(1,:),param_(2,:),signs_{bs});
end;
legend('Sample', 'Subject');
xlabel('\omega_0');
ylabel('\omega_1');
display('The subject-level bootstrapping has slightly larger variability in the parametric space');

%% View the variability due to logistic regression in the raw score space
title_bs = {'Sample-level bootstrap', 'Subject-level bootstrap'};
for bs=[SAMPLE_BS, SUBJECT_BS]
  param_ = model{b_}{bs,LOGISTIC}.param;
  for s=1:size(param_,2)
    Prob(:,s) = glmval(param_(:,s), xspace, 'logit');
  end;
  figure(bs); clf; hold on;
  plot(xspace, Prob);
  title(title_bs{bs});
end;

%% View the variability due to logistic regression in the likelihood space

for bs=[SAMPLE_BS, SUBJECT_BS]
  likelihoods_LOGISTIC{b_,bs} = infer_LOGISTIC_xspace(model{b_}{bs,LOGISTIC}.param, xspace);
end;

close all;
figure(1); hold on;

for bs=[SAMPLE_BS, SUBJECT_BS]
  plot_likelihood_xspace(xspace, likelihoods_LOGISTIC{b_,bs},bs-1);
end;
legend('sample','sample (median)','sample','subject','subject (median)','subject', 'location', 'southeast');
display('The subject-level bootstrapping has slightly larger confidence intervals.');

%%
close all; 
b_=4; % experiment 4 out of 13 is chosen
p = info(b_,1);
b = info(b_,2);
figure(1); %subplot(3,1,[1 2]);
xbound_error_{1} = plot_histogram(xspace, data{p}.dset{1,2}(:,b), data{p}.dset{1,1}(:,b));
xlabel('');
printfig('histogram04', 'main_llr_confidence_tutorial');

figure(2); %subplot(3,1,[1 2]);
xbound_error_{2} = plot_histogram(xspace, data{p}.dset{1,2}(:,b)+1, data{p}.dset{1,1}(:,b)-1);
xlabel('');
printfig('histogram04_plus1', 'main_llr_confidence_tutorial');

%[~, ~,com_lr] = calibrate_logistic2(data{p}.dset{1,1}(:,b),data{p}.dset{1,2}(:,b),1);
%%
%close all;
clear com_kde;
[~, ~,com_kde{1}{1}] = calibrate_LLR(data{p}.dset{1,1}(:,b),data{p}.dset{1,2}(:,b),1);
[~, ~,com_kde{2}{1}] = calibrate_LLR(data{p}.dset{1,1}(:,b)-1,data{p}.dset{1,2}(:,b)+1,1);
fnames_ ={'KDE04', 'KDE04_plus1'};
for m=1:2
  figure(m); clf; %subplot(3,1,3);cla; 
  hold on;
  likelihoods_{m} = infer_KDE_xspace(com_kde{m}, xspace);
  plot(xspace, likelihoods_{m} / log(10));
  xlabel('score');
  ylabel('log_{10} LR');
  axis_=axis;
  h = area(xbound_error_{m}, [axis_(3) axis_(3)], 'LineStyle',':', 'basevalue', axis_(4));
  h(1).FaceColor = [0.5 0.5 0]; alpha(.3);
  plot(axis_(1:2), [-5 -5],'k-.');
  plot(axis_(1:2), [+5 +5],'k:');
  legend('LR', 'Overlap','-5','+5','location','southeast');
  printfig(fnames_{m}, 'main_llr_confidence_tutorial');
end
%%
figure(1);
printfig('histogram04', 'main_llr_confidence_tutorial');
figure(2);
printfig('histogram04_plus1', 'main_llr_confidence_tutorial');


%%
%Get the wer function from 
addpath(genpath('../BiometricExperimentDesignTutorial2016/wer'));

%%
for b_=1:13
  p = info(b_,1);
  b = info(b_,2);
  eer(b_) = wer(data{p}.dset{1,1}(:,b),data{p}.dset{1,2}(:,b)); 
end

%% check for normality of the distribution
for b_=1:13
  p = info(b_,1);
  b = info(b_,2);
  for k=1:2
    data_ = data{p}.dset{1,k}(:,b);
    m_ = mean(data_);  s_ = std(data_);
    zscores_ = (data_ - m_) ./ s_;
    [h(b_,k),pValue(b_,k),ksstat(b_,k),cv(b_,k)] = kstest(zscores_);
  end
end
%%
clf; set(gca,'fontsize',12);hold on;
barh(ksstat); 
set(gca,'ytick',1:13);
set(gca,'yticklabel',syslabel);
axis_ = axis;
plot([cv(1,1), cv(1,1)], axis_(3:4),'k--');
plot([cv(1,2), cv(1,2)], axis_(3:4),'r-.');
legend('diff-pair','same-pair','cv diff-pair','cv same-pair');
xlabel('KS statistics');
set(gca,'fontsize',12);
%%
printfig('ksstat','main_plot_figure1');

%% Demonstrate the posterior distribution
% first check the likelihood ratio
figure(4); clf;
plot_likelihood_xspace(xspace, likelihoods_LOGISTIC{b_,SUBJECT_BS},0);
plot_likelihood_xspace(xspace, likelihoods{b_,SUBJECT_BS},1);
%%
close all;
llh_space=linspace(-10,10,1000);
figure(1);
[posterior_LOGISTIC] = plot_posterior_likelihood_xspace(xspace, likelihoods_LOGISTIC{b_,SUBJECT_BS}, llh_space);
%
figure(2);
[posterior_KDE] = plot_posterior_likelihood_xspace(xspace, likelihoods{b_,SUBJECT_BS}, llh_space);
%%
flist = {'posterior_LR', 'posterior_KDE'};
for f=1:2
  figure(f);
  axis tight;
  %axis([-3 3 -10 10]);
  %printfig(flist{f},'main_plot_figure1');
end
%%
close all;
xlist = linspace(0,5,4);
subplot(4,4,[1:12]);
plot_likelihood_xspace(xspace, likelihoods_LOGISTIC{b_,SUBJECT_BS},0);
plot_likelihood_xspace(xspace, likelihoods{b_,SUBJECT_BS},1);

axis_=axis;
for x=1:numel(xlist)
  plot([xlist(x) xlist(x)], axis_(3:4),'k:');
end
%
for x=1:numel(xlist)
  [~,ii_x] = min(abs(xlist(x)-xspace));
  subplot(4,4,12+x); cla; hold on;
  plot(llh_space, posterior_LOGISTIC(ii_x,:),'b-');
  plot(llh_space, posterior_KDE(ii_x,:),'r-.');
  title(sprintf('E_{*} = %1.1f',xlist(x)));
  set(gca,'fontsize',12);
end
xlabel('Log_{10} LR');
ylabel('Probability of log_{10} LR');
legend('LR','KDE','location','northwest')

%%
printfig('posterior_demo','main_plot_figure1');

%%
b_= 4;
p = info(b_,1);
b = info(b_,2);

[t.dset{1,1}, t.dset{1,2}, com ] = calibrate_logistic2(data{p}.dset{1,1}(:,b), data{p}.dset{1,2}(:,b),1 );
llr_scores = glmval(com.b, xspace, 'identity');
figure(1);
[bound, logLR_ELUB] = apply_ELUB(llr_scores , t.dset{1,2}, t.dset{1,1},1 );
%%
figure(2); clf;hold on;
plot(xspace, llr_scores /log(10),'g-','linewidth',3); %in log base 10
plot(xspace, logLR_ELUB /log(10),'r--'); %in log base 10
xlabel('score');
ylabel('Log_{10} LR');
set(gca,'fontsize',12);
