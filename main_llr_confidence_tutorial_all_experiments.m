%% Run all the 13 experiments.
% Author: Norman Poh (normanpoh@ieee.org)
% Date  : 2017-06-05

%% First load the data
load main_llr_confidence.mat

%% Add path
addpath Lib

% Get the configuration for the 13 experiments
b_=0;
for p=1:2
  for b=1:size(data{p}.dset{1,1},2)
    b_=b_+1;
    info(b_,:) = [p b];
  end;
end;

%% Run the bootstrapping procedure across the 13 experiments
clear com model;
LOGISTIC=1;
KDE=2;
SAMPLE_BS=1;
SUBJECT_BS=2;

if exist('main_llr_confidence_tutorial_all_experiments.mat') %we run this only once
  load main_llr_confidence_tutorial_all_experiments
else
  for b_=1:13
    p = info(b_,1);
    b = info(b_,2);
    
    fprintf(1,'Running experiment %s, based on Lausanne Protocol %d, experiment %d\n', ...
      syslabel{b_}, p, b);
    
    data_ = [data{p}.dset{1,1}(:,b);data{p}.dset{1,2}(:,b)];
    xspace_{b_} = linspace(min(data_), max(data_), 1000);
    
    tic;
    for s = 1:100 %Perform 100 bootstraps, each time fitting using methods 1 or 20
      selected_user = randi(200,1,200);
      nexpe = filter_users(data{p}, b, selected_user);
      
      % METHOD 1: logistic regression
      [~, ~,com_] = calibrate_logistic2(nexpe.dset{1,1}, nexpe.dset{1,2},1);
      model{b_}{SUBJECT_BS,LOGISTIC}.param(:,s) = com_.b;
      
      % METHOD 2: KDE
      [~, ~,com_] = calibrate_LLR(nexpe.dset{1,1}, nexpe.dset{1,2},1);
      model{b_}{SUBJECT_BS,KDE}{s} = com_;
      fprintf(1,'.');
      
      % ELUB applied to
      %likelihoods_LOGISTIC{b_,SUBJECT_BS} = infer_LOGISTIC_xspace(model{b_}{SUBJECT_BS,LOGISTIC}.param, xspace_{b_});
      %logLR_ELUB = apply_ELUB(data_, logLR_train_s, logLR_train_d)
    end;
    fprintf(1,'\n');toc
  end;
  
  save main_llr_confidence_tutorial_all_experiments.mat model xspace_
end

%% Make inference
for b_=1:13
  likelihoods_LOGISTIC{b_,SUBJECT_BS} = infer_LOGISTIC_xspace(model{b_}{SUBJECT_BS,LOGISTIC}.param, xspace_{b_});
  likelihoods_KDE{b_,SUBJECT_BS} = infer_KDE_xspace(model{b_}{SUBJECT_BS,KDE}, xspace_{b_});
end;

%% Analyse the results of all experiments and do the plotting
%close all;
grp_consistent = zeros(1000,13);
grp_overlap = zeros(1000,13);
grp_ELUB = zeros(1000,13);
for b_=1:13
  p = info(b_,1);
  b = info(b_,2);

  xspace__=xspace_{b_};
  
  %figure(1);
  %figure('units','normalized','outerposition',[0 0 1 1])
  clf; set(gca,'fontsize',12);

  subplot(5,1,5); set(gca,'fontsize',12);
  %plot_histogram(xspace_{b_}, data{p}.dset{1,2}(:,b), data{p}.dset{1,1}(:,b), xbound_);
  xbound_error = plot_histogram(xspace__, data{p}.dset{1,2}(:,b), data{p}.dset{1,1}(:,b));

  subplot(5,1,[1 2 3]); cla; hold on; set(gca,'fontsize',12);
  ptile{LOGISTIC} = plot_likelihood_xspace(xspace__, likelihoods_LOGISTIC{b_,SUBJECT_BS},0);
  ptile{KDE} = plot_likelihood_xspace(xspace__, likelihoods_KDE{b_,SUBJECT_BS},1);
  
  %ELUB
  [t.dset{1,1}, t.dset{1,2}, com ] = calibrate_logistic2(data{p}.dset{1,1}, data{p}.dset{1,2}, b);
  llr_scores = glmval(com.b, xspace__, 'identity');
  [logLR_ELUB] = apply_ELUB(llr_scores , t.dset{1,2}, t.dset{1,1},2);
  
  plot(xspace__, logLR_ELUB/log(10),'g--','linewidth',3);
  ylabel('Log_{10} LR');
  
  %shade the zone
  axis_=axis;
  xbound_ELUB = get_xbound_ELUB(xspace__, logLR_ELUB, axis_);

  % Compute the overlap and does the plotting at the same time
  axis_ = axis; xlim_ = axis_(1:2);
  [jaccard_,~,union_, xbound_consistent] = calculate_intervals_overlap(...
    ptile,axis_,xspace__, 0.5, xbound_error);
  xlabel('');
  title(syslabel{b_});
  ylim([-5, 5]);
  set(gca,'ytick', -5:1:5);
    
  legend('logistic','logistic (median)','logistic','KDE','KDE (median)','KDE', ...
    'logistic ELUB', 'ELUB region', 'consistent region','location', 'northwest');
  title(syslabel{b_});
  
  
  subplot(5,1,4); set(gca,'fontsize',12);
  plot(xspace__,jaccard_);
  ylabel('Jaccard');
  xlim(xlim_);

  subplot(5,1,5);
  xlim(xlim_);
  
  similarity(b_) = jaccard_similiarity(xbound_error, xbound_consistent);

  %collect info to analyse the Jaccard distribution for each experiment
  grp_consistent(:,b_) = analyse_jaccard_distribution(xspace__, jaccard_, xlim_, xbound_consistent);
  grp_overlap(:,b_) = analyse_jaccard_distribution(xspace__, jaccard_, xlim_, xbound_error);
  grp_ELUB(:,b_) = analyse_jaccard_distribution(xspace__, jaccard_, xlim_, xbound_ELUB);
  jaccard(:,b_)= jaccard_;
  
  drawnow;
  fname = sprintf('Expe%02d_wELUB', b_);
  %printfig(fname);
  %pause;
end;
%% plot the Jaccard distribution for each experiment
for b_=1:13
  subplot(3,5, b_); 
  %boxplot(jaccard(:,b_), grp_consistent(:,b_));
  boxplot( [ jaccard(:,b_); jaccard(:,b_); jaccard(:,b_)], [ grp_consistent(:,b_); grp_overlap(:,b_)+2; grp_ELUB(:,b_)+4]);
  set(gca,'xtick',1:6);
  set(gca,'xticklabel',{'iC','oC','iO','oO','iE','oE'});
  title(syslabel{b_});
  axis([0.5000    6.5000   0 1]);
  set(gca,'ytick',0:0.2:1);
  if b_==1
    ylabel('Jaccard');
  end
  set(gca,'fontsize',12);
end
%%
subplot(3,5,14);cla;
axis off;
space_ =linspace(0.9, 0.1, 6);

text_ = {'iC = inside Consistent region',...
  'oC = outside Consistent region',...
  'iO = inside Overlapping region',...
  'oO = outside Overlapping region',...
  'iE = inside ELUB region',...
  'oE = outside ELUB region'};

for s=1:numel(text_)
  text(0,space_(s),text_{s},'fontsize',12);
end
%%
printfig('jaccard_distribution_wELUB');