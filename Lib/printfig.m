function printfig(fname, mScript_name)
%print the currently selected figure
% Author: Norman Poh (normanpoh@ieee.org)
% Date  : 2017-06-05

if ~exist('Pictures', 'dir')
  mkdir Pictures
end;

if nargin<2
  mScript_name = 'main_llr_confidence_tutorial_all_experiments';
end;

fname_=sprintf('Pictures/%s__%s', fname, mScript_name);
print('-dpng', [fname_ '.png']);
print('-depsc2', [fname_ '.eps']);
savefig(fname_);