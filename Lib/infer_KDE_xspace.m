function likelihood = infer_KDE_xspace(model, xspace)
% This function infers the trained KDE using its model
% See also infer_KDE_xspace.
%
% Author: Norman Poh (normanpoh@ieee.org)
% Date  : 2017-06-05

for s=1:numel(model)
  %com = model{b_}{SAMPLE_BS,KDE}{s};
  com = model{s};
  f_{1} = interp1(com.x,com.pdf{1},xspace,'linear');
  f_{2} = interp1(com.x,com.pdf{2},xspace,'linear');
  likelihood(:,s)  = log( f_{2}+realmin) - log (f_{1}+realmin);
end;    
