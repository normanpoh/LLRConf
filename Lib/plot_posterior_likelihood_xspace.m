function [posterior, llh_space]= plot_posterior_likelihood_xspace(xspace, likelihood, llh_space, plot_raw_curves)
% This function plots the posterior probability of the likelihood ratio
% sampled at llh_space. Only KDE is implemented
% The variable likelihood holds a set of bootstrapped likelihood curves.
% This function is not yet thoroughly tested and could be buggy.
%
% Author: Norman Poh (normanpoh@ieee.org)
% Date  : 2017-06-05

if nargin<4 || isempty(plot_raw_curves)
  plot_raw_curves=1;
end;

if nargin<3 || isempty(llh_space)
  llh_space = linspace(-10,10,1000); %llh is in log_10 scale!
end

posterior = zeros(size(likelihood,2),1000);
for x_ = 1:size(likelihood,1)
  if mean(isnan(likelihood(x_,:) )) == 1
    continue;
  end
  posterior(x_,:) = ksdensity(likelihood(x_,:) / log(10), llh_space);
  posterior(x_,:) = posterior(x_,:) /sum(posterior(x_,:));
end;

if plot_raw_curves
  [xtesta1,xtesta2]=meshgrid(xspace, llh_space );
  [na,nb]=size(xtesta1);
  xtest1=reshape(xtesta1,1,na*nb);
  xtest2=reshape(xtesta2,1,na*nb);
  xtest=[xtest1;xtest2]';
  
  ypredmat=reshape(posterior,na,nb);
  hold off
  %contourf(xtesta1,xtesta2,ypredmat,50);shading flat;
  surf(xtesta1,xtesta2,ypredmat); %,50);
  shading flat;
  view(2);
  axis tight;
  xlabel('Score');
  ylabel('Log_{10} LR');
  zlabel('Posterior prob of Log_{10} LR');
  set(gca,'fontsize',12);
  h=colorbar;
  ylabel(h,'Posterior prob of Log_{10} LR');
  set(h,'fontsize',12);
end
