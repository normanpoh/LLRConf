function ptile = plot_likelihood_xspace(xspace, likelihood, signs_type, plot_raw_curves)
% This function plots the likelihood ratio of the xspace (the score space)
% The variable likelihood holds a set of bootstrapped likelihood curves.
% 
% Author: Norman Poh (normanpoh@ieee.org)
% Date  : 2017-06-05

if nargin<3 || isempty(signs_type)
  signs_type=0;
end;

if nargin<4 || isempty(plot_raw_curves)
  plot_raw_curves=0;
end;


if signs_type==0
  signs_ptile = {'b--', 'b-', 'b--'};
else
  signs_ptile = {'r--', 'r-', 'r--'};
end;

% get the percentile
for x_ = 1:size(likelihood,1)
  ptile(x_,:)=prctile(likelihood(x_,:)/ log(10), [2.5, 50, 97.5]);
end;

% plotting
hold on;
if plot_raw_curves
  for s=1:size(likelihood,1)
    plot(xspace, likelihood(:,s));
  end;
end;

if plot_raw_curves
  linewidth = [2 4 2]; 
else
  linewidth = [1 2 1];
end;

for r=1:3
  plot(xspace, ptile(:,r), signs_ptile{r}, 'linewidth',linewidth(r));
end;
xlabel('score');
ylabel('Log_{10} LR');

%find visible axis in x
visible_x = ~isnan(sum(ptile,2));
axis_ = axis;
minx_ = min(xspace(visible_x));
maxx_ = max(xspace(visible_x));
miny_ = -10;
maxy_ = +10;

%axis([minx_ maxx_ axis_(3:4)]);
axis([minx_ maxx_ miny_ maxy_]);
