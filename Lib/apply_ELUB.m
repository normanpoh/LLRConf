function [logLR_ELUB, log_ELUB] = apply_ELUB(logLR_test, logLR_train_s, logLR_train_d, draw)
% calculates empirical lower and upper bounds (ELUB) using logLR_train_s, logLR_train_d
% and applies the bounds to logLR_test
% input and output are natual log likelihood ratios
% see Vergeer et al (2016) http://dx.doi.org/10.1016/j.scijus.2016.06.003
% logLR_train_s is the same-pair scores
% logLR_train_d is the different-pair scores
% Note: 
% 1. Scores must be logLR
% 2. logLR_test is in natural log
% 3. For display (draw=1), however, we use logLR_test that is in log base 10.
%
% Author: Norman Poh (normanpoh@ieee.org)
% Date  : 2017-06-05


if nargin<4
  draw=0;
end

N_CLMR = 1; %
logLRth_step = .01; %

logLR_ELUB = logLR_test;

logLR_train_s = sort(logLR_train_s);
logLR_train_d = sort(logLR_train_d);

n_s = length(logLR_train_s);
n_d = length(logLR_train_d);

logLRth_min = min([logLR_train_s(1) logLR_train_d(1)]);
logLRth_max = max([logLR_train_s(end) logLR_train_d(end)]);
logLRth = [logLRth_min:logLRth_step:logLRth_max];
LRth = exp(logLRth);
length_LRth = length(LRth);

ELUB = NaN(length_LRth,1);
parfor I_LRth = 1:length_LRth % recode to make more efficient?
  if LRth(I_LRth)>1
    ELUB_numerator = 1;
  else
    ELUB_numerator = LRth(I_LRth);
  end
  
  n_LR_s_lessthan_LRth = sum(logLR_train_s<=logLRth(I_LRth));
  n_LR_d_greaterthan_LRth = sum(logLR_train_d>logLRth(I_LRth));
  
  ELUB_denominator_part_1 = (N_CLMR + n_LR_s_lessthan_LRth) / (N_CLMR + n_s);
  ELUB_denominator_part_2 = LRth(I_LRth) * (N_CLMR + n_LR_d_greaterthan_LRth) / (N_CLMR + n_d);
  
  ELUB(I_LRth) = ELUB_numerator / (ELUB_denominator_part_1 + ELUB_denominator_part_2);
end
log_ELUB = log(ELUB);
II_log_ELUB = log_ELUB>0;
if sum(II_log_ELUB) == 0
  logLR_ELUB(:) = 0; % at no point was this system better than a system that always returns a likelihood ratio of 1
else
  %         % the following searched for the first point from the middle which is below 0
  %         [max_log_ELUB, I_max_ELUB] = max(log_ELUB);
  %         I_EUB = find(II_log_ELUB(I_max_ELUB:end) == 0, 1) + I_max_ELUB - 1;
  %         I_ELB = find(II_log_ELUB(1:I_max_ELUB) == 0, 1, 'last');
  %         if isempty(I_EUB), I_EUB = length_LRth; end
  %         if isempty(I_ELB), I_ELB = 1; end
  %         log_EUB(I_sample) = logLRth(I_EUB);
  %         log_ELB(I_sample) = logLRth(I_ELB);
  % the following is simpler and finds the values furthest from the middle
  log_UB = max(logLRth(II_log_ELUB));
  log_LB = min(logLRth(II_log_ELUB));
  
  II_above_UB = logLR_test > log_UB;
  logLR_ELUB(II_above_UB) = log_UB;
  II_below_LB = logLR_test < log_LB;
  logLR_ELUB(II_below_LB) = log_LB;
end
switch draw
  case 1
    plot(logLRth/log(10),log_ELUB);
    hold on
    plot([logLRth_min/log(10), logLRth_max/log(10)], [0,0], 'k--');
    axis tight;
    axis_=axis;
    plot([log_LB log_LB] /log(10),axis_(3:4),'k:');
    plot([log_UB log_UB] /log(10),axis_(3:4),'k:');
    hold off
    %fprintf('%g\t%g\n', log_LB, log_UB);
    legend('ELUB criterion','0','Bound');
    xlabel('Log_{10} LR');
    ylabel('ELUB criterion');
    set(gca,'fontsize',12);
  otherwise
    %do nothing
end
%bound = [log_LB log_UB];