function xbound_error = plot_histogram(xspace, same_scores, diff_scores, xbound)
% This function plots the histogram will a bounding box overlayed on top
% The bound is given in xbound.
%
% Author: Norman Poh (normanpoh@ieee.org)
% Date  : 2017-06-05

if nargin<4
  xbound=[];
end

min_score = min(xspace);
max_score = max(xspace);
num_bins = 100;
bin_width = (max_score-min_score)/(num_bins-1);
bin_edges = min_score : bin_width : max_score;

hist_d = histogram(diff_scores, bin_edges, 'Normalization', 'pdf', 'FaceColor', 'r', 'EdgeColor', 'r', 'FaceAlpha', 0.25);
hold on
hist_s = histogram(same_scores, bin_edges, 'Normalization', 'pdf', 'FaceColor', 'b', 'EdgeColor', 'b', 'FaceAlpha', 0.25);
ylabel('prob. density');
xlabel('score');
set(gca, 'Layer', 'top');
xlim([min_score max_score]);


%overlay the limit of the empirical observation
axis_=axis;
xbound_error = [min(same_scores), max(diff_scores)];
h = area(xbound_error, [axis_(3) axis_(3)], 'LineStyle',':', 'basevalue', axis_(4));
h(1).FaceColor = [0.5 0.5 0];
alpha(.3);
%plot([max(diff_scores) max(diff_scores)], axis_(3:4),'k--');
%plot([min(same_scores) min(same_scores)], axis_(3:4),'k--');
axis(axis_);
legend('Diff pair','Same pair','Overlap');

%overlay the upper and lower limit
if ~isempty(xbound)
  h = area([xbound(1), xbound(2)], [axis_(3) axis_(3)], 'LineStyle',':', 'basevalue', axis_(4));
  h(1).FaceColor = [0 0.25 0.25];
  alpha(.5);
  legend('Diff pair','Same pair','Overlap','Consistent region');
end
