function nexpe = filter_users(expe, chosen, user_seq)
% filter selected users
% Author: Norman Poh (normanpoh@ieee.org)
% Date  : 2017-06-05

if nargin<2|length(chosen)==0
  chosen = 1;
end;

model_ID = unique(expe.label{1,1});

for d=1:2,for k=1:2
    nexpe.dset{d,k} = [];
    nexpe.label{d,k} = [];
  end;
end;

%for id=1:size(model_ID,1),
for id=user_seq,
  %get the data set associated to the ID
  %fprintf(1,'%d\n',id);
  for d=1:2,for k=1:2
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      tmp{d,k} = expe.dset{d,k}(index{d,k},chosen);
      nexpe.dset{d,k} = [nexpe.dset{d,k}; tmp{d,k}];
      nexpe.label{d,k} = [nexpe.label{d,k}; expe.label{d,k}(index{d,k})];
    end;
  end;
end;