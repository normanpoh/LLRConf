function group = analyse_jaccard_distribution(xspace_, jaccard_, xlim_, xbound_, draw)
% group = analyse_jaccard_distribution(xspace_, jaccard_, xlim_, xbound_, draw)
% xsace_ and jaccard must have the same length
% xbound_ can either be xbound_consistent or xbound_error
%
% output is group containing either 1 or 2, and may be zero
% 1 is inside the region
% 2 is outside the region
% 0 is undefined/unclassified
%
% Author: Norman Poh (normanpoh@ieee.org)
% Date  : 2017-06-05

%In the xspace, the indices below are ordered in the following way
% ii_limit(1), ii_xbound(1), ii_xbound(2), ii_limit(2)
if numel(xspace_) ~= numel(jaccard_)
  error('xspace_ and jaccard must have the same length');
end

if nargin < 5
  draw=0;
end
  
[~,ii_limit(1)]=min(abs(xspace_-xlim_(1)));
[~,ii_limit(2)]=min(abs(xspace_-xlim_(2)));
[~,ii_xbound(1)]=min(abs(xspace_-xbound_(1)));
[~,ii_xbound(2)]=min(abs(xspace_-xbound_(2)));

ii_outside = [ii_limit(1):ii_xbound(1) ii_xbound(2):ii_limit(2)];
ii_inside = [ii_xbound(1):ii_xbound(2)];

group=zeros(size(xspace_));
group(ii_inside)=1;
group(ii_outside)=2;

if sum(group==0)
  warning('xspace has points unaccounted for');
end

if draw
  boxplot(jaccard_, group);
end
%jaccard_(ii_outside);
%jaccard_(ii_inside);

  