function similarity = jaccard_similiarity(xboundA, xboundB)
% This function computes the Jaccard similarity given two bounds or
% intervals. 
% I could have implemented this more efficiently but opted for simplicity
% instead.
%
% Author: Norman Poh (normanpoh@ieee.org)
% Date  : 2017-06-05

A =round([min(xboundA) max(xboundA)] * 1000);
B = round([min(xboundB) max(xboundB)] * 1000);
A_ = A(1):A(2);
B_ = B(1):B(2);
intersect_ = numel(intersect(A_, B_));
union_ = numel(union(A_, B_));
similarity = intersect_ / union_;