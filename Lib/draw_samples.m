function nexpe = draw_samples(expe, chosen)
% draw samples from the expe object.
% Author: Norman Poh (normanpoh@ieee.org)
% Date  : 2017-06-05

if nargin<2||isempty(chosen)
  chosen = 1;
end;

for d=1,for k=1:2
    nrows = size(expe.dset{d,k} , 1);
    selected = randi(nrows, 1, nrows);
    
    nexpe.dset{d,k} = expe.dset{d,k}(selected, chosen);
    nexpe.label{d,k} = expe.label{d,k}(selected);
  end;
  
  d=2; %do nothing on the test data
  nexpe.dset{d,k} = expe.dset{d,k}(:,chosen);
  nexpe.label{d,k} = expe.label{d,k};
  
end;
