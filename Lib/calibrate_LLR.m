function [imp_calib, gen_calib, com ] = calibrate_LLR(imp_, gen_, chosen, debug)
% [imp_calib, gen_calib, com ] = calibrate_logistic2(imp, gen)
% Given a pair of inputs (imp, gen) scores, each of which must be one 
% column, this funciton gives a calibrated output.
%
% [imp_calib, gen_calib, com ] = calibrate_logistic2(imp, gen, chosen)
% chosen specifies the column of data to use
%
% Author: Norman Poh (normanpoh@ieee.org)
% Date  : 2017-06-05

if nargin<3||isempty(chosen),
  chosen=1;
end;

if nargin<4||isempty(debug),
  debug=0;
end;

%%
dat_ = [imp_(:,chosen); gen_(:,chosen)];
x = linspace(min(dat_), max(dat_),100);

[com.pdf{1},com.x]=ksdensity(imp_(:,chosen) ,x);
[com.pdf{2},com.x]=ksdensity(gen_(:,chosen) ,x);

%f_= interp1(com.verif_x,com.verif_pdf{GENUINE});

% for the impostor output
f_{1} = interp1(com.x,com.pdf{1},imp_(:,chosen),'linear');
f_{2} = interp1(com.x,com.pdf{2},imp_(:,chosen),'linear');
imp_calib  = log( f_{2}+realmin) - log (f_{1}+realmin);

% for the genuine output
f_{1} = interp1(com.x,com.pdf{1},gen_(:,chosen),'linear');
f_{2} = interp1(com.x,com.pdf{2},gen_(:,chosen),'linear');
gen_calib  = log( f_{2}+realmin) - log (f_{1}+realmin);

%% STOP here

if debug,
  subplot(2,1,1);
  cla; hold on;
  plot(com.x, com.pdf{1},'r--');
  plot(com.x, com.pdf{2},'b--');
  grid on;
  f_{1} = interp1(com.x,com.pdf{1},com.x,'linear');
  f_{2} = interp1(com.x,com.pdf{2},com.x,'linear');
  subplot(2,1,2);
  llh_  = log( f_{2}+realmin) - log (f_{1}+realmin);
  plot(com.x, llh_);
  grid on;
%   subplot(3,1,2);
%   wer(imp_(:,chosen),gen_(:,chosen),[],4)
% 
%   subplot(3,1,3);
%   wer(imp_calib(:,chosen),gen_calib(:,chosen),[],4)
%   
end
