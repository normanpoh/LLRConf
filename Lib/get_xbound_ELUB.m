function xbound_ELUB = get_xbound_ELUB(xspace__, logLR_ELUB, axis_)
% This function finds the corresponding original score space where ELUB
% occurs.
% Author: Norman Poh (normanpoh@ieee.org)
% Date  : 2017-06-05

if nargin<3
  axis_ =[];
end

%find out what xbound_ELUB is in the original space
ii_xbound_ELUB = [ max(find(min(logLR_ELUB) == logLR_ELUB))  min(find(max(logLR_ELUB) == logLR_ELUB)) ];
xbound_ELUB = xspace__(ii_xbound_ELUB);

if ~isempty(axis_)
  h = area([xbound_ELUB], [axis_(3) axis_(3)], 'LineStyle',':', 'basevalue', axis_(4));
  h(1).FaceColor = [1 0 0];  alpha(.5);
end