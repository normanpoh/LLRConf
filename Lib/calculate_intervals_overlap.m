function [overlap_, intersect_, union_, xspace_lim] = calculate_intervals_overlap(...
  ptile, axis_, xspace, thrd, xspace_bound, face_color)
% [overlap_, intersect_, union_, xspace_lim] = CALCULATE_INTERVALS_OVERLAP(ptile, axis_, xspace, thrd, xspace_bound)
% This function takes in two percentiles list of Nrow by 6, which are given
% by the commands
% E.g.,
% ptile{LOGISTIC} = plot_likelihood_xspace(xspace, likelihoods_LOGISTIC{b_,SUBJECT_BS},0);
% ptile{KDE} = plot_likelihood_xspace(xspace, likelihoods{b_,SUBJECT_BS},1);
% axis_ = axis;
% % Then call
% [overlap_,~,union_, xbound_] = calculate_intervals_overlap(ptile,axis_,xspace_{b_}, 0.5, xbound_error);
% If axis_ is not provided, this function will not plot the bounding area.
% thrd for the Jaccard index is 0.5 by default
% if xbound_error is supplied, it constrains the search space to the the
% xspace bound [min max]. The bound is provided by the overlapping region 
% of the same-pair and diff-pair scores.
%
% Author: Norman Poh (normanpoh@ieee.org)
% Date  : 2017-06-05

if nargin<2
  axis_=[];
end;

if nargin<4
  thrd=0.5; % we use the default threshold of 0.5 for the overlapping region
end;

if nargin<5
  xspace_bound=[]; % we use the default threshold of 0.5 for the overlapping region
end;

if nargin<6
  face_color=[0 0.25 0.25]; % we use the default threshold of 0.5 for the overlapping region
end;

% compute the Jaccard index
ptile_round= round([ptile{1} ptile{2}] * 1000);
for row=1:size(ptile_round,1)
  intersect_(row) = numel(intersect(ptile_round(row, 1):ptile_round(row, 3), ptile_round(row, 4):ptile_round(row, 6)));
  union_(row) = numel(union(ptile_round(row, 1):ptile_round(row, 3), ptile_round(row, 4):ptile_round(row, 6)));
end;
overlap_ = intersect_ ./ union_;

if ~isempty(xspace_bound) %constrain the search space to the the xspace bound supplied
  min_ = min(find(overlap_>thrd & xspace >= min(xspace_bound)));
  max_ = max(find(overlap_>thrd & xspace <= max(xspace_bound)));
else
  min_ = min(find(overlap_>thrd));
  max_ = max(find(overlap_>thrd));
end

if ~isempty(axis_) % overlay the shaded area using the axis information supplied
  % Plot the shaded area
  h = area([xspace(min_), xspace(max_)], [axis_(3) axis_(3)], 'LineStyle',':', 'basevalue', axis_(4));
  h(1).FaceColor = face_color;
  alpha(.3);
end;
xspace_lim = [xspace(min_) xspace(max_)];