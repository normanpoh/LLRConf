function likelihood = infer_LOGISTIC_xspace(param, xspace)
% This function infers the trained logistic regression using its model
% parameters.
% See also infer_KDE_xspace.
%
% Author: Norman Poh (normanpoh@ieee.org)
% Date  : 2017-06-05

for s=1:size(param,2)
  likelihood(:,s) = glmval(param(:,s), xspace, 'identity');
end;